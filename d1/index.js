console.log("Hello Gel!")

// Array Methods
// JS has built in functions and methods for arrays. This allows us to manipulate and access array items 

// Mutator methods
/*
	- mutator methods are functions that "mutate" or change an array afetr thay are created
	- this methods manipulate the original array performing various tasks such as adding and removing elements
*/


let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
	
	// push()
	/*
		- adss an element in the end of an array and returns the array's length
		- can add 1 or multiple
		SYNTAX - arrayName.push();
	*/
	console.log('Current Array: ');
	console.log(fruits); //logs fruits array
	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength); //5
	console.log('Mutated array from push method');
	console.log(fruits); //Mango is added at the end of the array

	fruits.push('Avocado', 'Guava');
	console.log("Mutated array from push method");
	console.log(fruits);

	// Pop()
	/*
		- removes the last element in an array and returns the removed element
		- SYNTAX arrayName.pop();
	*/

	let removeFruit = fruits.pop();
	console.log(removeFruit); //Guava
	console.log('Mutated array from pop method');
	console.log(fruits); //Guava is removed

	// Mini activity 1

	let ghostFighters = ["Eugene", "Dennis", "Alfred", "Taguro"]
	console.log(ghostFighters);

	// let removeFriend = ghostFighters.pop();
	// console.log(removeFriend);
	// console.log('A friend has been removed');
	// console.log(ghostFighters);

	// Solution

	function unfriend(){
		ghostFighters.pop()
	}
	unfriend();
	console.log(ghostFighters);

	// unshift()
	/*
		- adds one or more elements at the beginning of an array
		- SYNTAX 
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB');
	*/

	fruits.unshift('Lime', 'Banana');
	console.log("Mutated array from unshift method:");
	console.log(fruits);

	// shift()
	/*
		- removes an element at the beginning of an array and returns the removed element
		- SYNTAX arrayName.shift();
	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method:");
	console.log(fruits);

	// splice
	/*
		simultaneously removes elements from a specified index number and add elements
		SYNTAX arrayName.splice(startingIndex, deleteCount,elementsToBeAdded);
	*/

	fruits.splice(1,2,'Lime','Cherry');
	console.log('Mutated array from Splice method');
	console.log(fruits);

	// sort
	/*
		- rearranges the array elements in alphanumerical order
		SYNTAX arrayName.sort();
	*/
		fruits.sort();
		console.log("Mutated array from sort method");
		console.log(fruits);


	// reverse
	/*
		- reverses the order of array elements
		SYNTAX arrayName.revers();
	*/

		fruits.reverse();
		console.log("Mutated array from reverse method");
		console.log(fruits);



	// Non Mutator methods
	/*
		- are functions that that do not modify or change an array after they are created
		- these do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

	// indexOf()
	/*
		- this returns the index number of the first matching element found in an array
		- if no match is found, the result is -1
		- the search process will be done from first element processing to the last element
		SYNTAX arrayName/indexOf(searchValue)
				arrayName/indexOf(searchValue,fromIndex);
	*/

	let firstIndex = countries.indexOf('PH',3);
	console.log("Result of indexOf method: " + firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log("Result of indexOf method: " + invalidCountry);

	// lastIndexOf()
	/*
		- this returns the index number of the last matching element found in an array
		- the search process will be done from the last element proceeding to the first element
		SYNTAX arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue,fromIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH',3);
	console.log('Result of lastIndexOf method: ' + lastIndex);


	// slice()
	/*
		- portions/slices elements from an array and returns a new array
		SYNTAX arrayName.slice(startingIndex);
				arrayName.slice(startingIndex,edningIndex);
	*/

	let slicedArrayA = countries.slice(2); //positive numbers starts in the beginning
	console.log("Result from slice method: ");
	console.log(slicedArrayA);

	let slicedArrayB = countries.slice(2,4);
	console.log("Result from slice method: ");
	console.log(slicedArrayB);

	let slicedArrayC = countries.slice(-4); // the negative numbers starts from the end fo the array
	console.log("Result from slice method: ");
	console.log(slicedArrayC);


	// toString()
	/*
		- returns an array as a string separated by commas
		SYNTAX arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("Result from toString method: ");
	console.log(stringArray);

	// concat()
	/*
		- combines two arrays or more and returns the combined result
		SYNTAX arrayA.concat(arrayB);
				arrayAconcat(elementA);
	*/

	let taskArrayA = ['drink html', 'eat javascript'];
	let taskArrayB = ['inhale css', 'breathe sass'];
	let taskArrayC = ['get git', 'be node'];

	let tasks = taskArrayA.concat(taskArrayB);
	console.log('Result from concat method');
	console.log(tasks);


	// combine multiple arrays

	console.log('Result from concat method');
	let allTasks = taskArrayB.concat(taskArrayB,taskArrayC);
	console.log(allTasks);

	// combine arrays with elements

	let combineTasks = taskArrayA.concat('smell express', 'throw react');
	console.log('Result from concat method');
	console.log(combineTasks);


	// join()
	/*
		- returns an array as a string separated by specified separator string
		SYNTAX arrayName.join('separatorString')
	*/

	let users = ["John", "Jane", "Joe", "Robert", "Nej"];
	console.log(users.join()); //default separator is comma
	console.log(users.join(''));
	console.log(users.join(' - '));

	// iteration method
	/*
		- are loops designed to perform repetitive tasks on arrays
		- loops over all items in an array
		- useful for manipulating array data resultig in complex tasks
	*/

	// forEach
	/*
		- similar to a for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		- the anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
		- variable names for arrays are normally written in the plural form of the data stored in an array 
		- its common practice to use the singular form of the array content for parameter names used in array loops
		- forEach() does NOT return anything
		SYNTAX arrayName.forEach(function(individualElement){
				statement
		})
	*/

	allTasks.forEach(function(task){ // can input anyting on the (task)
		console.log(task);
	});



	// Mini Activity 2

	function characters(){
		ghostFighters.forEach(function(task){ 
		console.log(task);
	})
	};
		characters();


	// using forEach with conditional statements
	// looping through all array items
	//  it is good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each of the loop
	// creating a separate variable to store of an array iteration
	// iteration methods are also good practice to avoid confusion by modifying the original array
	// mastering loops and arrays allows us developers to perform a wide range of features that help in data management and analysis

	let filteredTasks = [];
	allTasks.forEach(function(task){
		console.log(tasks);
		if(task.length>10){
			console.log(task);
			filteredTasks.push(task)
		}
	});
	console.log("Result of filtered task: ");
	console.log(filteredTasks);


	// map()
	/*
		- iterates on each element and returns new array with different values  depending on the results of the function's operation
		- requires the use of return keyword
		- map returns a new array, forEach does not
		SYNTAX let/conts resultArray = arrayName.map(function(indvidualElement))
	*/

	let numbers = [1,2,3,4,5];
	let numberMap = numbers.map(function(number){
		return number * number;
	});
	console.log("Original Array: ");
	console.log(numbers);
	console.log("Result of map method: ");
	console.log(numberMap);
	


	// map() vs forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;
	});
	console.log(numberForEach);


	// every()
	/*
		- checks if all elements in an array meet the given conditions
		- this is useful for validating data stored in arrays especially when dealing with huge or large amounts of data
		- retruns true if all elements meet the condition, otherwise false
		SYNTAX let/const resultArray = arrayName.every(function(individualElement){
			return expression/condition;
		})
	*/

	let allValid = numbers.every(function(number){
		return (number < 3);
	})
	console.log("Result of every method: ");
	console.log(allValid);


	// some()
	/*
		- checks if at least one element in the array meets the given condition
		- returns the value if at least one element meets the condition and false if otherwise
		SYNTAX let/const resultArray = arrayName.some(function(individualElement){
			return expression/condition;
		})
	*/

	let someValid = numbers.some(function(number){
		return (number<2);
	});
	console.log("The result of some method: ")
	console.log(someValid); 


	// combining the returned result from every/some method maybe used in other statements to perform consecutive results
	if(someValid){
		console.log('Some numbers in the array are greater than 2'); // it returns bec the value of someValid is true
	};

	// filter
	/*
		- returns a new array that contains elements which meets the given condition
		- returns an empty array if no elements were found
		SYNTAX 
		let/const resultArray = arrayName.filter(function(individualElement){
			return expression/condition;
		})
	*/

	let filterValid = numbers.filter(function(number){
		return (number<3);
	});
	console.log("The result of filter method: ")
	console.log(filterValid); 


	let nothingFound = numbers.filter(function(number){
		return (number = 0)
	});
	console.log("The result of filter method: ")
	console.log(nothingFound); // will return empty bec it does not match anythng within the array



	// Filtering using forEach

	let filteredNumbers = [];

	numbers.forEach(function(number){
		// console.log(number);
		if(number<3){
			filteredNumbers.push(number);
		}
	})
	console.log("The result of filter method: ")
	console.log(filteredNumbers); 



	// includes()
	/*
		- checks if the argument passed can be found in the array
		- it returns a boolean which can also be saved in a variable
		- returns true if the argument is found the array
		- returns false if it is not
		SYNTAX 
		arrayName.includes(<argumentToFind>)
	*/

	let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let productFound = products.includes("Mouse");
	console.log(productFound);

	let productNotFound = products.includes("Headset");
	console.log(productNotFound);

	// Method Chaining
		// Methods can be chained using them one after another
		// The result of the first method is used on the second method until all "chained" methods have been resolved

		let filterProducts = products.filter(function(product){
			return product.toLowerCase().includes('a');
		})
		console.log(filterProducts);

		

		// Mini activity 3

		let contacts = ["Ash"];

		// function addTrainer(trainer){
		// 	let name1 = "Eugene"
		// 	console.log(name1);
		// 	let name2 = "Alfred"
		// 	console.log(name2);
		// 	let name3 = "Dennis"
		// 	console.log(name3);

		// }
		// addTrainer();

		// solution

		function addTrainer(trainer){
			let doesTrainerExist = contacts.includes(trainer);
			if(doesTrainerExist){
				alert("Already added")
			}else{
				contacts.push(trainer);
				alert("Registered");
			}
		};

		// // Reduce
		
		// 	- evaluates elements from left to right and returns/reduces the array into a single value
		// 	SYNTAX
		// 		let/const resultArray = arrayName.reduce(function(accumulator,currentValue){
		// 			return expression/operation
		// 		})

		// 		- "accumulator" parameter in the function stores the result for every iteration of the loop 
		// 		- "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop 

		// 		HOW the "reduce" method works
		// 		1. the first result/element in the array is stored in "accumulator" parameter
		// 		2. the second/next element in the array is stored in the current value parameter
		// 		3. an operation is performed on the two elements
		// 		4. the loop repeats step 1-3 until all elements....
		


		console.log(numbers);

		let iteration = 0;
		let iterationStr = 0;

		let reducedArray =numbers.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration)
			console.log("accumulator: " + x);
			console.log('current value: ' + y);
			return x + y;
		})

		console.log("Result of reduce method: " + reducedArray); //15
		// [1,2,3,4,5] = 1+1 = 2, 

		let list = ["Hello", "Again", "World"];
		let reduceJoin = list.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration)
			console.log("accumulator: " + x);
			console.log('current value: ' + y);
			return x + y;
		})
		console.log("Result of reduce method: " + reduceJoin);







	



